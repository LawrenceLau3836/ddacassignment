﻿using DDACAssignment.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DDACAssignment.Data
{
    public class UserRoleSeeder
    {
        public static async Task UserRoleGenerator(ApplicationDbContext context,
            UserManager<UserProfile> userManager, RoleManager<IdentityRole> roleManager)
        {
            //Ensure database is created
            context.Database.EnsureCreated();
            //Your role to add here
            string[] Roles = { "Admin", "User" };

            foreach (string Role in Roles)
            {
                if (!await roleManager.RoleExistsAsync(Role))
                {
                    IdentityRole newRole = new IdentityRole(Role);
                    await roleManager.CreateAsync(newRole);
                    await PreAdmin(userManager);
                }
            }
        }

        public static async Task PreAdmin(UserManager<UserProfile> userManager)
        {
            UserProfile Admin = new UserProfile();
            Admin.Email = "lawrence3836@gmail.com";
            Admin.UserName = "lawrence3836@gmail.com";
            Admin.PhoneNumber = "0123456789";
            Admin.FName = "Lawrence";
            Admin.LName = "Lau";
            Admin.Gender = "Male";
            Admin.DateOfBirth = DateTime.Parse("9/10/1997 00:00:00 AM");
            await userManager.CreateAsync(Admin,".Law3836");
            await userManager.AddToRoleAsync(Admin, "Admin");
        }
    }
}
