﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DDACAssignment.Data.Migrations
{
    public partial class PetSupplies : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PetSupply",
                columns: table => new
                {
                    ItemID = table.Column<string>(nullable: false),
                    ItemName = table.Column<string>(nullable: true),
                    ItemQuantity = table.Column<int>(nullable: false),
                    ItemDescription = table.Column<string>(nullable: true),
                    Price = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PetSupply", x => x.ItemID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PetSupply");
        }
    }
}
