﻿using System;
using System.Collections.Generic;
using System.Text;
using DDACAssignment.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DDACAssignment.Data
{
    public class ApplicationDbContext : IdentityDbContext<UserProfile>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<DDACAssignment.Models.PetSupply> PetSupply { get; set; }
    }
}
