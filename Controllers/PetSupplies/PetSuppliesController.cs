﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DDACAssignment.Data;
using DDACAssignment.Models;

namespace DDACAssignment.Controllers.PetSupplies
{
    public class PetSuppliesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PetSuppliesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: PetSupplies
        public async Task<IActionResult> Index()
        {
            return View(await _context.PetSupply.ToListAsync());
        }

        // GET: PetSupplies/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var petSupply = await _context.PetSupply
                .FirstOrDefaultAsync(m => m.ItemID == id);
            if (petSupply == null)
            {
                return NotFound();
            }

            return View(petSupply);
        }

        // GET: PetSupplies/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: PetSupplies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ItemID,ItemName,ItemQuantity,ItemDescription,Price")] PetSupply petSupply)
        {
            if (ModelState.IsValid)
            {
                _context.Add(petSupply);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(petSupply);
        }

        // GET: PetSupplies/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var petSupply = await _context.PetSupply.FindAsync(id);
            if (petSupply == null)
            {
                return NotFound();
            }
            return View(petSupply);
        }

        // POST: PetSupplies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("ItemID,ItemName,ItemQuantity,ItemDescription,Price")] PetSupply petSupply)
        {
            if (id != petSupply.ItemID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(petSupply);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PetSupplyExists(petSupply.ItemID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(petSupply);
        }

        // GET: PetSupplies/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var petSupply = await _context.PetSupply
                .FirstOrDefaultAsync(m => m.ItemID == id);
            if (petSupply == null)
            {
                return NotFound();
            }

            return View(petSupply);
        }

        // POST: PetSupplies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var petSupply = await _context.PetSupply.FindAsync(id);
            _context.PetSupply.Remove(petSupply);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PetSupplyExists(string id)
        {
            return _context.PetSupply.Any(e => e.ItemID == id);
        }
    }
}
