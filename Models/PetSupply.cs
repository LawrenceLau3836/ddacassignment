﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DDACAssignment.Models
{
    public class PetSupply
    {
        [Key]
        public string ItemID { get; set; }
        public string ItemName { get; set; }
        public int ItemQuantity { get; set; }
        public string ItemDescription { get; set; }
        public decimal Price { get; set; }
    }
}
