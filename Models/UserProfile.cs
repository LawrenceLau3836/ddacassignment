﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DDACAssignment.Models
{
    public class UserProfile : IdentityUser
    {
        public string FName { get; set; }
        public string LName { get; set; }
        public string Gender { get; set; }
        [NotMapped]
        public List<SelectListItem> Genders { get; set; }
        public DateTime DateOfBirth { get; set; }
        public override string PhoneNumber { get; set; }
    }
}
